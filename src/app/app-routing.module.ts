import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CharactersComponent} from './characters/characters.component';
import {HomeComponent} from './home/home.component';
import {CharacterNewComponent} from './characters/character-new/character-new.component';
import {CharacterDetailComponent} from './characters/character-detail/character-detail.component';
import {CharacterResolverService} from './characters/character-resolver.service';
import {CharactersSelectComponent} from './characters/characters-select/characters-select.component';

const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {
    path: 'characters',
    component: CharactersComponent,
    children: [
      {path: '', component: CharactersSelectComponent},
      {path: 'new', component: CharacterNewComponent},
      {path: ':id', component: CharacterDetailComponent, resolve: [CharacterResolverService]},
    ]
  },
  {
    path: 'home',
    component: HomeComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
