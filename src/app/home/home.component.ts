import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  onGodDamn(): void{
    const audio = new Audio();
    audio.src = '../assets/noob-noob-god-damn.mp3';
    audio.load();
    audio.play();
  }
}
