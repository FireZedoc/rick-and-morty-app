import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CharactersComponent } from './characters/characters.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { SharedComponent } from './shared/shared.component';
import { CharacterListComponent } from './characters/character-list/character-list.component';
import { CharacterPersComponent } from './characters/character-list/character-pers/character-pers.component';
import { CharacterDetailComponent } from './characters/character-detail/character-detail.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { CharacterNewComponent } from './characters/character-new/character-new.component';
import { CharactersSelectComponent } from './characters/characters-select/characters-select.component';
import {CharacterService} from './characters/character.service';
import {HttpClientModule} from '@angular/common/http';
import {FilterPipe} from './filter.pipe';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    CharactersComponent,
    HomeComponent,
    SharedComponent,
    CharacterListComponent,
    CharacterPersComponent,
    CharacterDetailComponent,
    CharacterNewComponent,
    CharactersSelectComponent,
    FilterPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [CharacterService],
  bootstrap: [AppComponent]
})
export class AppModule { }
