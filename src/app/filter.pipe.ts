import {Pipe, PipeTransform} from '@angular/core';
import {Character} from './characters/character.model';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform{
  transform(items: Character[], searchText: string): Character[] {
    if (!items){
      return [];
    }
    if (!searchText){
      return items;
    }
    return items.filter(it => {
      return it.name.toLocaleLowerCase().includes(searchText.toLocaleLowerCase());
    });
  }
}
