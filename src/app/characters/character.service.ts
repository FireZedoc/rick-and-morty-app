import {Character} from './character.model';
import {Injectable, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {DataStorageService} from '../shared/data-storage.service';

@Injectable()
export class CharacterService{
  characterSubject = new Subject<Character[]>();

  constructor(private dataStorageService: DataStorageService) {
  }

  private characters: Character[];

  fetchCharacters(): void{
    this.dataStorageService.fetchCharacters().subscribe(characters => {
      this.characters = characters;
    });
    this.characterSubject.next(this.characters.slice());
  }

  getCharacters(): Character[]{
    return this.characters.slice();
  }

  getCharacter(id: number): Character{
    return this.characters.slice()[id];
  }

  deleteCharacter(id: number): void{
    this.characters.splice(id, 1);
    this.characterSubject.next(this.characters.slice());
    this.dataStorageService.storeCharacters(this.getCharacters().slice());
  }

  addCharacter(character: Character): void{
    this.characters.push(character);
    this.characterSubject.next(this.characters.slice());
    this.dataStorageService.storeCharacters(this.getCharacters().slice());
  }
}
