export class Character {
  public name: string;
  public description: string;
  public imagePath: string;
  public intelligence: number;

  constructor(name: string, description: string, imagePath: string, intelligence: number) {
    this.name = name;
    this.description = description;
    this.imagePath = imagePath;
    this.intelligence = intelligence;
  }
}
