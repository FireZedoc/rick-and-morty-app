import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {CharacterService} from '../character.service';

@Component({
  selector: 'app-character-new',
  templateUrl: './character-new.component.html'
})
export class CharacterNewComponent implements OnInit {
  characterForm: FormGroup;

  constructor(private route: ActivatedRoute,
              private characterService: CharacterService,
              private router: Router) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.initForm();
    });
  }

  initForm(): void{
    this.characterForm = new FormGroup({
      name: new FormControl('', Validators.required),
      imagePath: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
      intelligence: new FormControl('', Validators.required),
    });
  }

  onCancel(): void{
    this.router.navigate(['../'], { relativeTo: this.route });
  }

  onSubmit(): void{
    this.characterService.addCharacter(this.characterForm.value);
    this.onCancel();
  }

}
