import {Component, OnInit} from '@angular/core';
import {CharacterService} from './character.service';
import {BrowserModule} from '@angular/platform-browser';

@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  providers: [CharacterService]
})
export class CharactersComponent implements OnInit{
  constructor(private characterService: CharacterService) {
  }
  ngOnInit(): void {
    this.characterService.fetchCharacters();
  }
}
