import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CharacterPersComponent } from './character-pers.component';

describe('CharacterPersComponent', () => {
  let component: CharacterPersComponent;
  let fixture: ComponentFixture<CharacterPersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CharacterPersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CharacterPersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
