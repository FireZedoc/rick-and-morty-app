import {Component, Input, OnInit} from '@angular/core';
import {Character} from '../../character.model';

@Component({
  selector: 'app-character-pers',
  templateUrl: './character-pers.component.html'
})
export class CharacterPersComponent implements OnInit {
  @Input() character: Character;
  @Input() index: number;

  ngOnInit(): void {
  }
}
