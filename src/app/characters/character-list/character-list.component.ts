import {Component, OnDestroy, OnInit} from '@angular/core';
import {Character} from '../character.model';
import {CharacterService} from '../character.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-character-list',
  templateUrl: './character-list.component.html'
})
export class CharacterListComponent implements OnInit, OnDestroy {
  characters: Character[];
  subscription: Subscription;
  searchText: string;

  constructor(private characterService: CharacterService,
              private route: ActivatedRoute,
              private router: Router) {}

  ngOnInit(): void {
    this.characters = this.characterService.getCharacters();
    this.subscription = this.characterService.characterSubject.subscribe(
      (characters: Character[]) => {
        this.characters = characters;
      }
    );
    this.characters = this.characterService.getCharacters();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  newCharacter(): void{
    this.router.navigate(['new'], {relativeTo: this.route});
  }
}
