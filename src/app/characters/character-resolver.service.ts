import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Character} from './character.model';
import {DataStorageService} from '../shared/data-storage.service';

@Injectable({providedIn: 'root'})
export class CharacterResolverService implements Resolve<Character[]>{
  constructor(private dataStorageService: DataStorageService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
    return this.dataStorageService.fetchCharacters();
  }
}
