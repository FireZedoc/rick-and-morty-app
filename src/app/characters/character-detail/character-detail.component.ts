import {Component, Input, OnInit} from '@angular/core';
import {Character} from '../character.model';
import {CharacterService} from '../character.service';
import {ActivatedRoute, Params, Router} from '@angular/router';

@Component({
  selector: 'app-character-detail',
  templateUrl: './character-detail.component.html'
})
export class CharacterDetailComponent implements OnInit {
  character: Character;
  idCharacter: number;

  constructor(private characterService: CharacterService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.idCharacter = + params.id;
      this.character = this.characterService.getCharacter(this.idCharacter);
    });
  }

  onDelete(): void {
    if (confirm('are you sure to delete' + this.character.name + '?')){
      this.characterService.deleteCharacter(this.idCharacter);
      this.router.navigate(['characters']);
    }
  }
}
