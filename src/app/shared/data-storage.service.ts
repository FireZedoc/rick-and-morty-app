import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Character} from '../characters/character.model';
import {Observable} from 'rxjs';

@Injectable({providedIn: 'root'})
export class DataStorageService {
  constructor(private httpClient: HttpClient) {
  }

  storeCharacters(characters: Character[]): void {
    this.httpClient.put(
      'https://rick-and-morty-app-4ead9.firebaseio.com/characters.json',
      characters
    ).subscribe(response => {
      console.log(response);
    });
  }

  fetchCharacters(): Observable<Character[]> {
    return this.httpClient.get<Character[]>(
      'https://rick-and-morty-app-4ead9.firebaseio.com/characters.json'
    );
  }
}
